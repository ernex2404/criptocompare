import React, { Component } from 'react';
import imagen from './cryptomonedas.png'
import Formulario from './componentes/Formulario';
import axios from 'axios'
import Resultado from './componentes/Resultado';

class App extends Component {

  state={
    resultado: {},
    monedaSeleccionada: '',
    criptoSeleccionada: ''
  }

  cotizarCriptomoneda = async (cotizacion) => {
    // obtener valores
    const {moneda, criptomoneda} = cotizacion

    // realizar consulta
    const url =`https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`

    // Realizar consulta
    await axios.get(url)
      .then(respuesta => {
        this.setState({
          resultado: respuesta.data.DISPLAY[criptomoneda][moneda]
        })
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <img src={imagen} alt="imagen" className="logotipo"/>
          </div>
          <div className="one-half column">
          <h1>Cotiza criptomonedas al instante</h1>
            <Formulario cotizarCriptomoneda={this.cotizarCriptomoneda} />

            <Resultado resultado={this.state.resultado}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
